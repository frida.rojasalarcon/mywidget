//
//  MyWidgetBundle.swift
//  MyWidget
//
//  Created by Frida Rojas Alarcon on 02/01/23.
//

import WidgetKit
import SwiftUI


struct MyWidgetBundle: WidgetBundle {
    var body: some Widget {
        HolaWidget()
    }
}
