//
//  WidgetAppApp.swift
//  WidgetApp
//
//  Created by Frida Rojas Alarcon on 02/01/23.
//

import SwiftUI

@main
struct WidgetAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
